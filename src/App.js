import React, { Component } from 'react';
import './App.css';

import Landing from './components/Landing/Landing';
import Modal from './components/Modal/Modal';

class App extends Component {
  state = { showModal: false }

  toggleModal = () => {
    this.setState({ showModal: !this.state.showModal }, () => {
      const node = document.querySelector('#modal');

      if (this.state.showModal) {
        node.classList.add('active')
        node.addEventListener('click', this.handleClick)
      } else {
        node.classList.remove('active')
        node.removeEventListener('click', this.handleClick);
      }

      
    })
  }

  handleClick = (e) => {
    if (e.target.id === 'modal') {
      this.toggleModal()
    }
  }
  render() {
    return (
      <>
        <Modal open={this.state.showModal} onClose={this.toggleModal} ></Modal>
        <Landing toggleModal={this.toggleModal} />
      </>
    )
  }
}

export default App;