import React, { Component } from 'react';
import './Seguimiento.css';
import Photo from '../../resources/images/seguimiento.jpg';

class Seguimiento extends Component {
    render() {
        return (
            <div className="seguimiento">
                <div className="text">
                    <h2>
                        Haz un seguimiento de las películas que ves
                    </h2>
                    <p className="desktop-only">
                        Cada vez que veas una película podrás marcarla como vista en la aplicación Moobee, ¡y a partir de aquí empieza la magia!
                    </p>
                    <p className="desktop-only">
                        Comparte qué te ha parecido la película con tus seguidores, recomiéndasela a un amigo o puntúala para que todos sepan si merece la pena o no. También podrás guardártela en una lista personalizada.  
                    </p>
                </div>
                <div className="image">
                    <img src={Photo} alt="Captura Moobee App"/>
                </div>
                <div className="text">
                { this.props.moviesSeen ? (<h3 className="highlight">¡{this.props.moviesSeen} películas vistas!</h3>) : null}
                </div>
            </div>
        );
    }
}

export default Seguimiento;